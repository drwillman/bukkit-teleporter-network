package me.pm.drwillman.bukkit.teleporterNetwork.util;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.java.JavaPlugin;

import me.pm.drwillman.bukkit.teleporterNetwork.Plugin;

public enum Recipes {
	ACTIVATOR {
		@Override
		public ShapedRecipe getRecipe() {
			ItemStack result = new ItemStack(Material.ENCHANTED_BOOK);
			ItemMeta itemMeta = result.getItemMeta();
			itemMeta.setDisplayName("Mad Dimensions");
			itemMeta.setLore(Arrays.asList("This seems to be an abridged edition.",
					"It retains most of the potency of the original.", ChatColor.ITALIC + "\"" + ChatColor.MAGIC
							+ "Thanos did nothing wrong." + ChatColor.RESET + ChatColor.ITALIC + "\""));

			itemMeta.getPersistentDataContainer().set(this.namespaceKey, PersistentDataType.INTEGER, 1);

			result.setItemMeta(itemMeta);

			ShapedRecipe recipe = new ShapedRecipe(this.namespaceKey, result);
			recipe.shape("* *", " B ", "* *");
			recipe.setIngredient('*', Material.GLOWSTONE_DUST);
			recipe.setIngredient('B', Material.BOOK);
			return recipe;
		}
	};

	NamespacedKey namespaceKey;

	Recipes() {
		this.namespaceKey = new NamespacedKey(JavaPlugin.getPlugin(Plugin.class),
				Recipes.class.getCanonicalName() + '.' + this.name());
	}

	public abstract ShapedRecipe getRecipe();

	public boolean constructed(ItemStack item) {
		return item != null && item.getItemMeta() != null
				&& item.getItemMeta().getPersistentDataContainer().has(this.namespaceKey, PersistentDataType.INTEGER);
	}

	// TODO Book with pages: CRC or hash the addresses and convert text
	// https://unicode-table.com/en/sets/fancy-letters/
}
