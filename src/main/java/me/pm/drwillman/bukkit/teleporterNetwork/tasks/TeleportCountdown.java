package me.pm.drwillman.bukkit.teleporterNetwork.tasks;

import java.util.Optional;

import org.bukkit.Sound;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import me.pm.drwillman.bukkit.teleporterNetwork.Plugin;
import me.pm.drwillman.bukkit.teleporterNetwork.model.Network;
import me.pm.drwillman.bukkit.teleporterNetwork.util.MetaData.MetaDataKeys;

public class TeleportCountdown extends BukkitRunnable {
	private static final int TICK_INTERVAL = 5; // ~0.5sec
	private static final int TP_DELAY_MS = 2000; // 2 sec
	private static final MetaDataKeys KEY = MetaDataKeys.PLAYER_TELEPORT_COUNTDOWN;

	private Player player;
	private String targetAddress;
	private long teleportAfterMs;

	/**
	 * Begin the countdown until player is teleported. Link the task to the player
	 * as metadata.
	 * 
	 * @param player
	 * @param target
	 */
	public static BukkitTask beginIfNotStarted(Player player, String targetAddress) {
		Optional<MetadataValue> countdown = KEY.getValueFor(player);
		BukkitTask task = null;

		if (!countdown.isPresent() || ((BukkitTask) countdown.get().value()).isCancelled()) {
			task = (new TeleportCountdown(player, targetAddress).runTaskTimer(JavaPlugin.getPlugin(Plugin.class),
					TICK_INTERVAL, TICK_INTERVAL));
			KEY.setValueFor(player, task);
		} else {
			task = (BukkitTask) countdown.get().value();
		}
		return task;
	}

	private TeleportCountdown(Player player, String targetAddress) {
		super();
		this.player = player;
		this.targetAddress = targetAddress;
		this.teleportAfterMs = System.currentTimeMillis() + TP_DELAY_MS;
	}

	@Override
	public void run() {
		Optional<MetadataValue> currentTargetAddress = MetaDataKeys.TELEPORTER_BLOCK
				.getValueFor((player.getLocation().getBlock().getRelative(BlockFace.DOWN)));

		if (currentTargetAddress.isPresent() && currentTargetAddress.get().asString().equals(this.targetAddress)) {
			// still on the teleporter after the countdown: teleport and clear the countdown
			if (System.currentTimeMillis() >= this.teleportAfterMs) {
				if (Network.teleportPlayer(this.player, this.targetAddress)) {
					this.player.playSound(player.getLocation(), Sound.ENTITY_EVOKER_CAST_SPELL, 10f, 2f);
				}
				KEY.removeFor(player);
				this.cancel();
			}
		}
		// left the teleporter or switched teleporters: restart the countdown
		else {
			KEY.removeFor(player);
			this.cancel();
		}
	}
}
