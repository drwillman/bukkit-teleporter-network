package me.pm.drwillman.bukkit.teleporterNetwork.listeners;

import java.util.Collection;
import java.util.stream.Collectors;

import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockCanBuildEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockFertilizeEvent;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.event.block.BlockMultiPlaceEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.block.EntityBlockFormEvent;
import org.bukkit.event.block.FluidLevelChangeEvent;

import me.pm.drwillman.bukkit.teleporterNetwork.util.MetaData.MetaDataKeys;

/**
 * Protect registered teleport zones
 */
public class Modification implements Listener {
	@EventHandler(priority = EventPriority.HIGHEST)
	public void preventBlockBreakEvent(BlockBreakEvent event) {
		prevent(event);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void preventBlockBurnEvent(BlockBurnEvent event) {
		prevent(event);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void preventBlockCanBuildEvent(BlockCanBuildEvent event) {
		prevent(event);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void preventBlockDamageEvent(BlockDamageEvent event) {
		prevent(event);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void preventBlockExplodeEvent(BlockExplodeEvent event) {
		prevent(event);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void preventBlockFertilizeEvent(BlockFertilizeEvent event) {
		prevent(event);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void preventBlockFormEvent(BlockFormEvent event) {
		prevent(event);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void preventBlockFromToEvent(BlockFromToEvent event) {
		prevent(event);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void preventBlockGrowEvent(BlockGrowEvent event) {
		prevent(event);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void preventBlockMultiPlaceEvent(BlockMultiPlaceEvent event) {
		prevent(event);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void preventBlockPistonExtendEvent(BlockPistonExtendEvent event) {
		prevent(event);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void preventBlockPistonRetractEvent(BlockPistonRetractEvent event) {
		prevent(event);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void preventBlockPlaceEvent(BlockPlaceEvent event) {
		prevent(event);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void preventBlockSpreadEvent(BlockSpreadEvent event) {
		prevent(event);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void preventEntityBlockFormEvent(EntityBlockFormEvent event) {
		prevent(event);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void preventFluidLevelChangeEvent(FluidLevelChangeEvent event) {
		prevent(event);
	}

	private void prevent(BlockEvent event) {
		if (event instanceof Cancellable) {
			// check to see if the block is protected
			boolean allowed = allowChange(event.getBlock());

			// if the event affects multiple blocks, consider the others as well
			if (allowed) {
				if (event instanceof BlockMultiPlaceEvent) {
					allowed = allowChanges(((BlockMultiPlaceEvent) event).getReplacedBlockStates().stream()
							.map(BlockState::getBlock).collect(Collectors.toSet()));
				} else if (event instanceof BlockFromToEvent) {
					allowed = allowChange(((BlockFromToEvent) event).getToBlock());
				} else if (event instanceof BlockPistonExtendEvent) {
					allowed = allowChanges(((BlockPistonExtendEvent) event).getBlocks());
				} else if (event instanceof BlockPistonRetractEvent) {
					allowed = allowChanges(((BlockPistonRetractEvent) event).getBlocks());
				}
			}

			// cancel the event if any of the blocks were protected
			if (!allowed) {
				((Cancellable) event).setCancelled(true);
			}
		}
	}

	private static boolean allowChanges(Collection<Block> blocks) {
		return blocks.stream().allMatch(block -> allowChange(block));
	}

	/**
	 * Check MetaData to see if this block is protected
	 * 
	 * @return true if block is unprotected (no MetaData flag set)
	 */
	private static boolean allowChange(Block block) {
		return !MetaDataKeys.PROTECTED_BLOCK.isPresentFor(block);
	}
}
