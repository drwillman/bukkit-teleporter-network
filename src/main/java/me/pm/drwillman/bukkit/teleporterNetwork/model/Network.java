package me.pm.drwillman.bukkit.teleporterNetwork.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import me.pm.drwillman.bukkit.teleporterNetwork.model.Teleporter.TeleporterType;
import me.pm.drwillman.bukkit.teleporterNetwork.tasks.ParticleEmitter;
import me.pm.drwillman.bukkit.teleporterNetwork.util.MetaData.MetaDataKeys;

public class Network {
	private static final Network SINGLETON = new Network();

	private static final TypeToken<List<Map<String, Object>>> SERIALIZED_TYPE = new TypeToken<List<Map<String, Object>>>() {
		private static final long serialVersionUID = 1L;
	};

	// hub address mapping to locations
	private Map<String, Location> addressRegistry = new HashMap<>();
	// link tp hub block to teleporter object
	private Map<Location, Teleporter> teleporterRegistry = new HashMap<>();
	private Consumer<Block> registerAction;
	private Consumer<Block> deregisterAction;

	private Network() {
		this.registerAction = block -> {
			ParticleEmitter.beginFor(block.getLocation());

			MetaDataKeys.PROTECTED_BLOCK.setValueFor(block, true);
			MetaDataKeys.PROTECTED_BLOCK.setValueFor(block.getRelative(BlockFace.UP), true);
			MetaDataKeys.PROTECTED_BLOCK.setValueFor(block.getRelative(BlockFace.UP, 2), true);
		};

		this.deregisterAction = block -> {
			ParticleEmitter.endFor(block.getLocation());

			MetaDataKeys.HUB_BLOCK.removeFor(block);
			MetaDataKeys.TELEPORTER_BLOCK.removeFor(block);
			MetaDataKeys.PROTECTED_BLOCK.removeFor(block);
			MetaDataKeys.PROTECTED_BLOCK.removeFor(block.getRelative(BlockFace.UP));
			MetaDataKeys.PROTECTED_BLOCK.removeFor(block.getRelative(BlockFace.UP, 2));
		};
	}

	public static void initialize(String serializedNetwork) {
		destroy();
		if (serializedNetwork != null && !serializedNetwork.isEmpty()) {
			(new Gson()).<List<Map<String, Object>>>fromJson(serializedNetwork, SERIALIZED_TYPE.getType()).stream()
					.map(Teleporter::deserialize).forEach(SINGLETON::registerTeleporter);
		}
	}

	public static String serialize() {
		String serialized = "";
		if (!SINGLETON.teleporterRegistry.isEmpty()) {
			List<Map<String, Object>> preSerialized = SINGLETON.teleporterRegistry.values().stream()
					.map(Teleporter::serialize).collect(Collectors.toList());

			serialized = (new Gson()).toJson(preSerialized);
		}

		return serialized;
	}

	public static void destroy() {
		SINGLETON.teleporterRegistry.values().stream().flatMap(Teleporter::streamAllComponents).map(Location::getBlock)
				.forEach(SINGLETON.deregisterAction);
		SINGLETON.teleporterRegistry = new HashMap<>();
		SINGLETON.addressRegistry = new HashMap<>();
	}

	/**
	 * Teleport the player to the given address
	 * 
	 * @param player
	 * @param address
	 */
	public static boolean teleportPlayer(Player player, String address) {
		boolean teleported = true;
		if (SINGLETON.addressExists(address)) {
			Location destination = SINGLETON.addressRegistry.get(address).clone().add(0.5, 1, 0.5);
			destination.setDirection(player.getLocation().getDirection());
			teleported = player.teleport(destination, TeleportCause.PLUGIN);
		} else {
			teleported = false;
		}

		return teleported;
	}

	/**
	 * Attempt to register a teleporter based on the the given hub
	 * 
	 * @param hub
	 * @param action an optional action to perform on each protected block, if
	 *               registration was successful
	 * @return true if the teleporter was registered by this call
	 */
	public static boolean tryRegisterTeleporter(Location hub) {
		boolean registered = false;
		// valid hub material and not already registered
		if (!SINGLETON.teleporterRegistry.containsKey(hub)) {
			Teleporter teleporter = Teleporter.tryCreate(hub);

			if (teleporter != null && !(teleporter.getType() == TeleporterType.ADDRESSABLE
					&& SINGLETON.addressExists(teleporter.getAddress()))) {
				SINGLETON.registerTeleporter(teleporter);
				registered = true;
			}
		}

		return registered;
	}

	/**
	 * Attempt to deregister the teleporter centered at the given hub block
	 * 
	 * @param hub
	 * @param action an optional action to perform on each protected block, if
	 *               deregistration was successful
	 * @return
	 */
	public static boolean tryDeregisterTeleporter(Location hub) {
		boolean deregistered = true;

		if (SINGLETON.teleporterRegistry.containsKey(hub)) {
			// if the hub has an address, remove it from the address registry
			MetaDataKeys.HUB_BLOCK.getValueFor(hub.getBlock()).ifPresent(metadataVal -> {
				if (metadataVal.value() != null) {
					SINGLETON.addressRegistry.remove(metadataVal.asString());
				}
			});

			SINGLETON.teleporterRegistry.remove(hub).streamAllComponents().map(Location::getBlock)
					.forEach(SINGLETON.deregisterAction);
		} else {
			deregistered = false;
		}

		return deregistered;
	}

	private boolean addressExists(String address) {
		return address != null && this.addressRegistry.containsKey(address);
	}

	private static String getAddressFor(Location hub, Stream<Location> members) {
		return members.map(location -> location.getBlock().getType().ordinal()).sorted().map(String::valueOf)
				.collect(Collectors.joining(";", String.valueOf(hub.getBlock().getType().ordinal()) + ":", ""));
	}

	private void registerTeleporter(Teleporter teleporter) {
		TeleporterType hubType = teleporter.getType();
		Location hub = teleporter.getHub();

		String hubAddress = hubType != TeleporterType.NONADDRESSABLE
				? getAddressFor(hub, teleporter.streamAddressComponents())
				: null;

		if (hubType == TeleporterType.ADDRESSABLE) {
			// mark the hub block
			MetaDataKeys.HUB_BLOCK.setValueFor(hub.getBlock(), hubAddress);
			this.addressRegistry.put(hubAddress, hub);
		} else {
			MetaDataKeys.HUB_BLOCK.setValueFor(hub.getBlock(), null);
		}

		teleporter.streamAllComponents().map(Location::getBlock).forEach(SINGLETON.registerAction);

		if (hubType == TeleporterType.SINGLE_LINK) {
			markLinkBlocks(hubAddress, teleporter.streamAllComponents());
		} else {
			// store target address for links
			teleporter.streamLinks().forEach(linkBlocks -> {
				String linkAddress = getAddressFor(hub, linkBlocks.stream());
				markLinkBlocks(linkAddress, linkBlocks.stream());
			});
		}

		this.teleporterRegistry.put(hub, teleporter);
	}

	private static void markLinkBlocks(String address, Stream<Location> link) {
		link.forEach(location -> MetaDataKeys.TELEPORTER_BLOCK.setValueFor(location.getBlock(), address));
	}
}
