package me.pm.drwillman.bukkit.teleporterNetwork;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Logger;

import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.base.Charsets;

import me.pm.drwillman.bukkit.teleporterNetwork.listeners.Activation;
import me.pm.drwillman.bukkit.teleporterNetwork.listeners.Modification;
import me.pm.drwillman.bukkit.teleporterNetwork.listeners.Registration;
import me.pm.drwillman.bukkit.teleporterNetwork.model.Network;
import me.pm.drwillman.bukkit.teleporterNetwork.util.Recipes;

public class Plugin extends JavaPlugin {
	@Override
	public void onEnable() {
		Logger log = getLogger();
		String serializedNetwork = null;
		File dataFolder = getDataFolder();

		if (!dataFolder.exists() && !dataFolder.mkdirs()) {
			log.warning("Unable to create data directory. Data will not persist.");
		} else {
			serializedNetwork = readFile(new File(dataFolder, "network.json"));
		}

		Network.initialize(serializedNetwork);
		getServer().getPluginManager().registerEvents(new Activation(), this);
		getServer().getPluginManager().registerEvents(new Registration(), this);
		getServer().getPluginManager().registerEvents(new Modification(), this);
		getServer().addRecipe(Recipes.ACTIVATOR.getRecipe());
	}

	@Override
	public void onDisable() {
		Logger log = getLogger();
		File dataFolder = getDataFolder();

		if (!dataFolder.exists() && !dataFolder.mkdirs()) {
			log.warning("Unable to create data directory. Data will not persist.");
		} else {
			writeFile(new File(dataFolder, "network.json"), Network.serialize());
		}
	}

	private String readFile(File file) {
		Logger log = getLogger();
		String result = null;
		if (file.isFile()) {
			try {
				log.info("Attempting to load file:" + file.toString());
				result = new String(Files.readAllBytes(file.toPath()), Charsets.UTF_8);
			} catch (IOException e) {
				log.severe("Failed to load file:" + file.toString());
				e.printStackTrace();
			}
		}

		return result;
	}

	private void writeFile(File file, String contents) {
		Logger log = getLogger();
		if (file.exists() && !file.canWrite()) {
			log.warning("Insufficient permissions to write to file:" + file.toString());
		}
		
		else if (file.isDirectory()) {
			log.warning("Unable to create file:" + file.toString());
			log.warning("A directory already exists with that name");
		}

		else {
			try {
				log.info("Attempting to write file:" + file.toString());
				Files.write(file.toPath(), contents.getBytes(Charsets.UTF_8));
			} catch (IOException e) {
				log.severe("Failed to write file:" + file.toString());
				e.printStackTrace();
			}
		}
	}
}
