package me.pm.drwillman.bukkit.teleporterNetwork.listeners;

import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import me.pm.drwillman.bukkit.teleporterNetwork.model.Network;
import me.pm.drwillman.bukkit.teleporterNetwork.tasks.Cooldown;
import me.pm.drwillman.bukkit.teleporterNetwork.util.Recipes;

public class Registration implements Listener {
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerInteractBlock(PlayerInteractEvent event) {
		if (event.getItem() != null && event.getClickedBlock() != null && Recipes.ACTIVATOR.constructed(event.getItem())
				&& !Cooldown.inProgressFor(event.getPlayer())) {
			World world = event.getPlayer().getWorld();

			// registered a teleporter
			if (Network.tryRegisterTeleporter(event.getClickedBlock().getLocation())) {
				world.playSound(event.getClickedBlock().getLocation(), Sound.ENTITY_PLAYER_BREATH, 10f, 0.5f);
			}

			// deregistered a teleporter
			else if (Network.tryDeregisterTeleporter(event.getClickedBlock().getLocation())) {
				world.playSound(event.getClickedBlock().getLocation(), Sound.ENTITY_SILVERFISH_STEP, 10f, 2f);
			}

			Cooldown.beginFor(event.getPlayer());
		}
	}
}
