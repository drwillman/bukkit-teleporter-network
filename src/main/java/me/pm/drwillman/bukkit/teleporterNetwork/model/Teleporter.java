package me.pm.drwillman.bukkit.teleporterNetwork.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

final class Teleporter {
	enum TeleporterType {
		SINGLE_LINK, ADDRESSABLE, NONADDRESSABLE
	}

	private static final Set<Material> PREFIX_MATS = EnumSet.of(Material.GOLD_BLOCK, Material.IRON_BLOCK,
			Material.NETHERITE_BLOCK);
	private static final Set<Material> ADDRESS_MATS = EnumSet.of(Material.BLACK_GLAZED_TERRACOTTA,
			Material.BLUE_GLAZED_TERRACOTTA, Material.BROWN_GLAZED_TERRACOTTA, Material.CYAN_GLAZED_TERRACOTTA,
			Material.GRAY_GLAZED_TERRACOTTA, Material.GREEN_GLAZED_TERRACOTTA, Material.LIGHT_BLUE_GLAZED_TERRACOTTA,
			Material.LIGHT_GRAY_GLAZED_TERRACOTTA, Material.LIME_GLAZED_TERRACOTTA, Material.MAGENTA_GLAZED_TERRACOTTA,
			Material.ORANGE_GLAZED_TERRACOTTA, Material.PINK_GLAZED_TERRACOTTA, Material.PURPLE_GLAZED_TERRACOTTA,
			Material.RED_GLAZED_TERRACOTTA, Material.WHITE_GLAZED_TERRACOTTA, Material.YELLOW_GLAZED_TERRACOTTA);

	private static final Set<BlockFace> HUB_SINGULAR_STRUCTURE = EnumSet.of(BlockFace.NORTH_WEST, BlockFace.NORTH_EAST,
			BlockFace.SOUTH_EAST, BlockFace.SOUTH_WEST);
	private static final Set<BlockFace> HUB_ADDRESS_STRUCTURE = EnumSet.of(BlockFace.NORTH, BlockFace.SOUTH,
			BlockFace.EAST, BlockFace.WEST);

	private TeleporterType type;
	private Location hub;
	private List<List<Location>> links;
	private List<Location> addressComponents;
	private List<Location> allComponents;

	static Teleporter tryCreate(Location hub) {
		boolean valid = true;
		TeleporterType type = null;
		List<Location> allComponents = null;
		List<List<Location>> links = null;
		List<Location> addressComponents = null;

		if (Teleporter.PREFIX_MATS.contains(hub.getBlock().getType())) {
			addressComponents = Teleporter.HUB_ADDRESS_STRUCTURE.stream()
					.map(face -> hub.getBlock().getRelative(face).getLocation()).collect(Collectors.toList());
			List<Material> hubAddressBlockMats = addressComponents.stream().map(Location::getBlock).map(Block::getType)
					.collect(Collectors.toList());

			if (Teleporter.ADDRESS_MATS.containsAll(hubAddressBlockMats)) {
				if (Teleporter.HUB_SINGULAR_STRUCTURE.stream().allMatch(
						face -> hub.getBlock().getRelative(face).getType().equals(hub.getBlock().getType()))) {
					type = TeleporterType.SINGLE_LINK;
				} else {
					type = TeleporterType.ADDRESSABLE;
				}
			} else if (Teleporter.PREFIX_MATS.containsAll(hubAddressBlockMats)) {
				type = TeleporterType.NONADDRESSABLE;
			} else {
				valid = false;
			}

			if (valid) {
				allComponents = new ArrayList<>();
				allComponents.add(hub);
				allComponents.addAll(addressComponents);

				if (type == TeleporterType.SINGLE_LINK) {
					allComponents.addAll(Teleporter.HUB_SINGULAR_STRUCTURE.stream()
							.map(face -> hub.getBlock().getRelative(face).getLocation()).collect(Collectors.toList()));
				} else if (type == TeleporterType.ADDRESSABLE || type == TeleporterType.NONADDRESSABLE) {
					links = getValidLinks(hub.getBlock());
					allComponents.addAll(links.stream().flatMap(List::stream).collect(Collectors.toList()));
				}

				if (!validateVerticalClearance(allComponents)) {
					valid = false;
				}
			}
		} else {
			valid = false;
		}

		return valid ? new Teleporter(type, hub, links, addressComponents, allComponents) : null;
	}

	Teleporter(TeleporterType type, Location hub, List<List<Location>> links, List<Location> addressComponents,
			List<Location> allComponents) {
		super();
		this.type = type;
		this.hub = hub;
		this.links = links != null ? links : Collections.emptyList();
		this.addressComponents = addressComponents;
		this.allComponents = allComponents;
	}

	public TeleporterType getType() {
		return type;
	}

	public Location getHub() {
		return hub;
	}

	public Stream<Location> streamAllComponents() {
		return allComponents.stream();
	}

	public Stream<List<Location>> streamLinks() {
		return links.stream();
	}

	public Stream<Location> streamAddressComponents() {
		return addressComponents.stream();
	}

	@SuppressWarnings("unchecked")
	static Teleporter deserialize(Map<String, Object> serializedTeleporter) {
		TeleporterType type = TeleporterType.valueOf((String) serializedTeleporter.get("type"));
		Location hub = Location.deserialize(((Map<String, Object>) serializedTeleporter.get("hub")));
		List<List<Location>> links = ((List<List<Map<String, Object>>>) serializedTeleporter.get("links")).stream()
				.map(list -> list.stream().map(Location::deserialize).collect(Collectors.toList()))
				.collect(Collectors.toList());

		List<Location> allComponents = new ArrayList<>();
		List<Location> addressComponents = Teleporter.HUB_ADDRESS_STRUCTURE.stream()
				.map(face -> hub.getBlock().getRelative(face).getLocation()).collect(Collectors.toList());

		allComponents.add(hub);
		allComponents.addAll(addressComponents);

		if (type == TeleporterType.SINGLE_LINK) {
			allComponents.addAll(Teleporter.HUB_SINGULAR_STRUCTURE.stream()
					.map(face -> hub.getBlock().getRelative(face).getLocation()).collect(Collectors.toList()));
		} else if (type == TeleporterType.ADDRESSABLE || type == TeleporterType.NONADDRESSABLE) {
			allComponents.addAll(links.stream().flatMap(List::stream).collect(Collectors.toList()));
		}

		return new Teleporter(type, hub, links, addressComponents, allComponents);
	}

	public Map<String, Object> serialize() {
		Map<String, Object> values = new HashMap<>();
		values.put("type", type.name());
		values.put("hub", hub.serialize());
		values.put("links",
				links.stream().map(list -> list.stream().map(Location::serialize).collect(Collectors.toList()))
						.collect(Collectors.toList()));
		return values;
	}

	public String getAddress() {
		return streamAddressComponents().map(location -> location.getBlock().getType().getKey().toString()).sorted()
				.collect(Collectors.joining(";", getPrefix(), ""));
	}

	private String getPrefix() {
		return hub.getBlock().getType().getKey().toString();
	}

	private static List<List<Location>> getValidLinks(Block hub) {
		List<List<Location>> linkBlocks = Arrays.asList(
				Arrays.asList(hub.getRelative(BlockFace.NORTH_WEST).getLocation(),
						hub.getRelative(BlockFace.NORTH_WEST, 2).getLocation(),
						hub.getRelative(BlockFace.NORTH_NORTH_WEST).getLocation(),
						hub.getRelative(BlockFace.WEST_NORTH_WEST).getLocation()),
				Arrays.asList(hub.getRelative(BlockFace.NORTH_EAST).getLocation(),
						hub.getRelative(BlockFace.NORTH_EAST, 2).getLocation(),
						hub.getRelative(BlockFace.NORTH_NORTH_EAST).getLocation(),
						hub.getRelative(BlockFace.EAST_NORTH_EAST).getLocation()),
				Arrays.asList(hub.getRelative(BlockFace.SOUTH_EAST).getLocation(),
						hub.getRelative(BlockFace.SOUTH_EAST, 2).getLocation(),
						hub.getRelative(BlockFace.SOUTH_SOUTH_EAST).getLocation(),
						hub.getRelative(BlockFace.EAST_SOUTH_EAST).getLocation()),
				Arrays.asList(hub.getRelative(BlockFace.SOUTH_WEST).getLocation(),
						hub.getRelative(BlockFace.SOUTH_WEST, 2).getLocation(),
						hub.getRelative(BlockFace.SOUTH_SOUTH_WEST).getLocation(),
						hub.getRelative(BlockFace.WEST_SOUTH_WEST).getLocation()));

		return linkBlocks.stream().filter(
				link -> link.stream().allMatch(block -> Teleporter.ADDRESS_MATS.contains(block.getBlock().getType())))
				.collect(Collectors.toList());
	}

	private static boolean validateVerticalClearance(List<Location> locations) {
		return locations.stream().map(Location::getBlock).allMatch(
				block -> block.getRelative(BlockFace.UP).isEmpty() && block.getRelative(BlockFace.UP, 2).isEmpty());
	}
}