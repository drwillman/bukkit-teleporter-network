package me.pm.drwillman.bukkit.teleporterNetwork.tasks;

import java.util.Optional;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import me.pm.drwillman.bukkit.teleporterNetwork.Plugin;
import me.pm.drwillman.bukkit.teleporterNetwork.util.MetaData.MetaDataKeys;

public class ParticleEmitter extends BukkitRunnable {
	private static final int TICK_INTERVAL = 5; // ~0.5sec
	private static final MetaDataKeys KEY = MetaDataKeys.PARTICLE_EMITTER;
	private static final int PARTICLE_COUNT = 3;
	private Location location;

	public static BukkitTask beginFor(Location location) {
		BukkitTask task = (new ParticleEmitter(location).runTaskTimer(JavaPlugin.getPlugin(Plugin.class), TICK_INTERVAL,
				TICK_INTERVAL));
		KEY.setValueFor(location.getBlock(), task);
		return task;
	}

	public static void endFor(Location location) {
		Optional<MetadataValue> task = KEY.getValueFor(location.getBlock());
		if (task.isPresent()) {
			((BukkitTask) task.get().value()).cancel();
		}
	}

	private ParticleEmitter(Location location) {
		super();
		this.location = location;
	}

	@Override
	public void run() {
		for (int i = 0; i < PARTICLE_COUNT; i++) {
			location.getWorld().spawnParticle(Particle.ENCHANTMENT_TABLE, location.getX() + Math.random(),
					location.getY() + 1, location.getZ() + Math.random(), 0, 0, 2, 0);
		}
	}
}
