package me.pm.drwillman.bukkit.teleporterNetwork.util;

import java.util.Optional;

import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.metadata.Metadatable;
import org.bukkit.plugin.java.JavaPlugin;

import me.pm.drwillman.bukkit.teleporterNetwork.Plugin;

public class MetaData {
	public enum MetaDataKeys {
		/**
		 * Blocks protected from being altered
		 */
		PROTECTED_BLOCK,

		/**
		 * Blocks that part of a teleporter link, holds addresses
		 */
		TELEPORTER_BLOCK,

		/**
		 * Single block per teleporter, at the center
		 */
		HUB_BLOCK,

		/**
		 * MS time after which the player will teleport
		 */
		PLAYER_TELEPORT_COUNTDOWN,

		/**
		 * MS time delay on register/deregister teleporter
		 */
		PLAYER_TELEPORTER_REGISTRATION_COOLDOWN,
		
		/**
		 * Emitter bound to a block
		 */
		PARTICLE_EMITTER;

		public boolean isPresentFor(Metadatable obj) {
			return obj.getMetadata(this.name()).stream()
					.filter(mdv -> mdv.getOwningPlugin().equals(JavaPlugin.getPlugin(Plugin.class))).findAny()
					.isPresent();
		}

		public Optional<MetadataValue> getValueFor(Metadatable obj) {
			return obj.getMetadata(this.name()).stream()
					.filter(mdv -> mdv.getOwningPlugin().equals(JavaPlugin.getPlugin(Plugin.class))).findAny();
		}

		public void setValueFor(Metadatable obj, Object value) {
			obj.setMetadata(this.name(), new FixedMetadataValue(JavaPlugin.getPlugin(Plugin.class), value));
		}

		public void removeFor(Metadatable obj) {
			obj.removeMetadata(this.name(), JavaPlugin.getPlugin(Plugin.class));
		}
	}
}
