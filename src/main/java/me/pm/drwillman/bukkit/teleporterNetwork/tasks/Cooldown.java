package me.pm.drwillman.bukkit.teleporterNetwork.tasks;

import java.util.Optional;

import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import me.pm.drwillman.bukkit.teleporterNetwork.Plugin;
import me.pm.drwillman.bukkit.teleporterNetwork.util.MetaData.MetaDataKeys;

public class Cooldown extends BukkitRunnable {
	private static final int TICK_INTERVAL = 5; // ~0.5sec
	private static final int COOLDOWN_MS = 1000; // 1 sec
	private static final MetaDataKeys KEY = MetaDataKeys.PLAYER_TELEPORTER_REGISTRATION_COOLDOWN;

	private Player player;
	private long endTime;

	public static BukkitTask beginFor(Player player) {
		BukkitTask task = (new Cooldown(player).runTaskTimer(JavaPlugin.getPlugin(Plugin.class), TICK_INTERVAL,
				TICK_INTERVAL));
		KEY.setValueFor(player, task);
		return task;
	}

	public static boolean inProgressFor(Player player) {
		Optional<MetadataValue> countdown = KEY.getValueFor(player);
		return countdown.isPresent() && !((BukkitTask) countdown.get().value()).isCancelled();
	}

	private Cooldown(Player player) {
		super();
		this.player = player;
		this.endTime = System.currentTimeMillis() + COOLDOWN_MS;
	}

	@Override
	public void run() {
		if (System.currentTimeMillis() >= this.endTime) {
			KEY.removeFor(player);
			this.cancel();
		}
	}

}
