package me.pm.drwillman.bukkit.teleporterNetwork.listeners;

import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import me.pm.drwillman.bukkit.teleporterNetwork.tasks.TeleportCountdown;
import me.pm.drwillman.bukkit.teleporterNetwork.util.MetaData.MetaDataKeys;

public class Activation implements Listener {
	/**
	 * Listen for player activation of a teleporter
	 * 
	 * @param event
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void activate(PlayerMoveEvent event) {
		if (!event.getFrom().toVector().equals(event.getTo().toVector())
				&& MetaDataKeys.TELEPORTER_BLOCK.isPresentFor(event.getTo().getBlock().getRelative(BlockFace.DOWN))) {
			TeleportCountdown.beginIfNotStarted(event.getPlayer(), MetaDataKeys.TELEPORTER_BLOCK
					.getValueFor(event.getTo().getBlock().getRelative(BlockFace.DOWN)).get().asString());
		}
	}
}
