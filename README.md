# TeleporterNetwork
A Bukkit plugin that allows a network of teleporters to be constructed.

## Features
* No commands required or used
* Address-based teleporter links
* Entrance/exit nodes may be added to the network in any order

## Dependencies
* Java 1.8
* Spigot 1.16.2

### Usage Overview
Each teleporter is constructed from a hub block combined with either four address blocks, or four of the same hub block material. The hub block can be any metal (gold/iron/netherite). The address blocks can be any glazed terracotta. This means the teleporter network is limited to 3 * (16 choose 4) exit nodes (i.e. 5460 addresses). There are no limits to the number of entrance nodes, beyond practical memory limits.

Blocks are placed in a horizontal plane. There must be 2 blocks of clearance (air) over each address/hub block. The teleporter is added to the network by using a crafted book on the center hub block.

There are 3 types of teleporter that can be constructed.
1. 1 exit node + 0-4 entrance nodes
2. No exit node + 0-4 entrance nodes
3. A compact form of a single entrance node

### Teleporter Structures
Where [A] is any entrance node address block, [E] is any exit node address block, and [H] the same hub block throughout the structure. [X] is any, or no block.
1 exit node with 4 entrance nodes:<br>
>AAXAA<br>
>AAEAA<br>
>XEHEX<br>
>AAEAA<br>
>AAXAA<br>

0 exit node with 4 entrance nodes:<br>
>AAXAA<br>
>AAHAA<br>
>XHHHX<br>
>AAHAA<br>
>AAXAA<br>

Compact, single entrance node:<br>
>HAH<br>
>AHA<br>
>HAH<br>

1 exit node with 2 entrance nodes:<br>
>XXXAA<br>
>XXEAA<br>
>XEHEX<br>
>AAEXX<br>
>AAXXX<br>

### Activator Item
A custom crafted item is used to register teleporters with the network. Where [B] is a book, [G] is glowstone dust, and [X] is empty:<br>
>GXG<br>
>XBX<br>
>GXG<br>
